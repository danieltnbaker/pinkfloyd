package dev.dandroid.pinkfloyd.ui.main

import android.os.Bundle
import dev.dandroid.pinkfloyd.R
import dev.dandroid.pinkfloyd.base.BaseActivity
import dev.dandroid.pinkfloyd.ui.list.AlbumListFragment

class MainActivity : BaseActivity() {

    override fun layoutRes(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.screenContainer, AlbumListFragment()).commit()
        }
    }
}