package dev.dandroid.pinkfloyd.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import dev.dandroid.pinkfloyd.data.domain.GetAlbumsUseCase
import dev.dandroid.pinkfloyd.data.model.Album
import javax.inject.Inject

class AlbumListViewModel @Inject constructor(private val getAlbumsUseCase: GetAlbumsUseCase) : ViewModel() {

    private var disposable = CompositeDisposable()

    val loadAlbums: MutableLiveData<List<Album>> = MutableLiveData()
    val loadError: MutableLiveData<Boolean> = MutableLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData()

    fun loadAlbums() {
        loading.value = true

        disposable.add(getAlbumsUseCase.getAlbums()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Album>>() {
                    override fun onSuccess(value: List<Album>) {
                        loadError.value = false
                        loadAlbums.value = value
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        loading.value = false
                    }
                }))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}