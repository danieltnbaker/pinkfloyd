package dev.dandroid.pinkfloyd.ui.list

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.album_list_fragment.*
import dev.dandroid.pinkfloyd.R
import dev.dandroid.pinkfloyd.base.BaseFragment
import dev.dandroid.pinkfloyd.util.ViewModelFactory
import javax.inject.Inject

class AlbumListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var albumListViewModel: AlbumListViewModel? = null

    override fun layoutRes(): Int = R.layout.album_list_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        albumListViewModel = ViewModelProviders.of(this, viewModelFactory).get(AlbumListViewModel::class.java)
        albumListViewModel?.let { viewModel ->

            viewModel.loadAlbums.observe(this, Observer {
                it?.let { albums ->
                    recyclerView.adapter = AlbumListAdapter(albums)
                    recyclerView.layoutManager = GridLayoutManager(context, 2)
                    recyclerView.visibility = View.VISIBLE
                }
            })
            viewModel.loadError.observe(this, Observer {
                if (it == true) {
                    recyclerView.visibility = View.GONE
                    errorTextView.visibility = View.VISIBLE
                    errorTextView.text = getString(R.string.album_list_error_message)
                } else {
                    errorTextView.visibility = View.GONE
                    errorTextView.text = null
                }
            })
            viewModel.loading.observe(this, Observer {
                if (it == true) {
                    swipeRefreshLayout.isRefreshing = true
                    recyclerView.visibility = View.GONE
                    errorTextView.visibility = View.GONE
                } else {
                    swipeRefreshLayout.isRefreshing = false
                }
            })

            swipeRefreshLayout.setOnRefreshListener { viewModel.loadAlbums() }

            viewModel.loadAlbums()
        }
    }
}