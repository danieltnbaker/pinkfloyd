package dev.dandroid.pinkfloyd.ui.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.album_list_item.view.*
import dev.dandroid.pinkfloyd.R
import dev.dandroid.pinkfloyd.data.model.Album
import dev.dandroid.pinkfloyd.ui.list.AlbumListAdapter.AlbumViewHolder

class AlbumListAdapter(val data: List<Album>) : RecyclerView.Adapter<AlbumViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.album_list_item, parent, false)
        return AlbumViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(album: Album) {
            itemView.albumTitle.text = album.title
            itemView.year.text = album.year.toString()

            Glide.with(itemView)
                .load(album.thumb)
                .centerInside()
                .placeholder(R.drawable.ic_album_placeholder)
                .into(itemView.albumArtwork)
        }
    }
}