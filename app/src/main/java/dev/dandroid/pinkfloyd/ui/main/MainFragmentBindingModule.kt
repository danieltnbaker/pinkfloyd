package dev.dandroid.pinkfloyd.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.pinkfloyd.ui.list.AlbumListFragment

@Module
abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun provideListFragment(): AlbumListFragment
}