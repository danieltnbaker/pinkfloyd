package dev.dandroid.pinkfloyd.data.rest

import dev.dandroid.pinkfloyd.data.model.Releases
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ArtistRepository @Inject constructor(private val releasesService: ArtistService) {

    fun getReleases(): Single<Releases> {
        return releasesService.getReleases()
                .subscribeOn(Schedulers.io())
    }
}