package dev.dandroid.pinkfloyd.data.domain

import dev.dandroid.pinkfloyd.data.mapper.AlbumMapper
import dev.dandroid.pinkfloyd.data.model.Album
import dev.dandroid.pinkfloyd.data.rest.ArtistRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetAlbumsUseCase @Inject constructor(private val artistRepository: ArtistRepository,
                                           private val albumMapper: AlbumMapper) {

    fun getAlbums(): Single<List<Album>> {
        return artistRepository.getReleases()
                .observeOn(Schedulers.computation())
                .map { albumMapper.mapToDomain(it) }
    }
}