package dev.dandroid.pinkfloyd.data.mapper

import dev.dandroid.pinkfloyd.data.model.Album
import dev.dandroid.pinkfloyd.data.model.Releases
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AlbumMapper @Inject constructor() {

    fun mapToDomain(releases: Releases): List<Album> {
        return releases.albums
    }
}