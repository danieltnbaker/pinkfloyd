package dev.dandroid.pinkfloyd.data.rest

import dev.dandroid.pinkfloyd.data.model.Releases
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ArtistService {

    @GET("artists/45467/releases")
    fun getReleases(
        @Query("per_page") count: Int = 50,
        @Query("sort_order") sortOrder: String = "desc"
    ): Single<Releases>
}