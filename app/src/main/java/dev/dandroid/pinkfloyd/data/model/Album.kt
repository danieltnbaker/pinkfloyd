package dev.dandroid.pinkfloyd.data.model

data class Album(
    val id: Int,
    val title: String,
    val thumb: String,
    val artist: String,
    val role: String,
    val year: Int,
    val resource_url: String,
    val type: String
)