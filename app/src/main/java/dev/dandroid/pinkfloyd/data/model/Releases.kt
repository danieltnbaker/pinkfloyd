package dev.dandroid.pinkfloyd.data.model

import com.google.gson.annotations.SerializedName

data class Releases(
    @SerializedName("releases")
    val albums: List<Album>
)