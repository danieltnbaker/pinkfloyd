package dev.dandroid.pinkfloyd.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dev.dandroid.pinkfloyd.base.BaseApplication
import dev.dandroid.pinkfloyd.di.module.ApplicationModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class
])
interface AppComponent : AndroidInjector<BaseApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: BaseApplication): Builder

        fun build(): AppComponent
    }
}