package dev.dandroid.pinkfloyd.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.pinkfloyd.di.util.ViewModelKey
import dev.dandroid.pinkfloyd.ui.list.AlbumListViewModel
import dev.dandroid.pinkfloyd.util.ViewModelFactory

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AlbumListViewModel::class)
    abstract fun bindListViewModel(albumListViewModel: AlbumListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}