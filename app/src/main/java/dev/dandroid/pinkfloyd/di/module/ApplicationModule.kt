package dev.dandroid.pinkfloyd.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dev.dandroid.pinkfloyd.base.BaseApplication
import javax.inject.Singleton

@Module(includes = [
    AndroidInjectionModule::class,
    NetworkModule::class,
    ViewModelModule::class,
    ActivityBindingModule::class
])
class ApplicationModule {

    @Provides
    @Singleton
    fun provideContext(application: BaseApplication): Context = application
}