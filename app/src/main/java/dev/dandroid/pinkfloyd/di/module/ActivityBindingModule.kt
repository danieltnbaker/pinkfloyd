package dev.dandroid.pinkfloyd.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.pinkfloyd.ui.main.MainActivity
import dev.dandroid.pinkfloyd.ui.main.MainFragmentBindingModule

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    abstract fun bindMainActivity(): MainActivity
}