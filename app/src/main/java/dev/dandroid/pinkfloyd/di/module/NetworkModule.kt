package dev.dandroid.pinkfloyd.di.module

import dagger.Module
import dagger.Provides
import dev.dandroid.pinkfloyd.data.rest.ArtistService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
                .client(OkHttpClient.Builder().build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitService(retrofit: Retrofit): ArtistService {
        return retrofit.create(ArtistService::class.java)
    }

    companion object {
        private const val BASE_URL: String = "https://api.discogs.com/"
    }
}